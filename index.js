const express = require('express');
const app = express();
const request = require('request-promise');
const querystring = require('querystring');

var flightApiRequest = request.defaults({
  baseUrl: 'http://node.locomote.com/code-task/'
});

app.get(['/airlines', '/airports'], (req, res, next) => {
  // I explictly chose not to use request.pipe() here as the documentation for
  // the request-promise package says usage of streaming is 'discouraged'. 
  // Otherwise I could have done flightApiRequest(req.originalUrl).pipe(res);
  flightApiRequest(req.originalUrl).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  });
});

app.get('/search', (req, res, next) => {
  flightApiRequest('/airlines').then((airlines) => {
    var qs = querystring.stringify(req.query);

    var requests = JSON.parse(airlines).map((airline) => {
      return flightApiRequest(`/flight_search/${airline.code}?${qs}`);
    });

    return Promise.all(requests);
  }).then((flights) => {
    var results = [];

    for (var i = 0; i < flights.length; ++i) {
      var json = JSON.parse(flights[i]);
      results = results.concat(json);
    }

    res.send(JSON.stringify(results));
  }).catch((err) => {
      next(err);
  });
});

app.use('/', express.static(__dirname + '/public'));

app.use((err, req, res, next) => {
  // Simplistic express error handling. For all errors, just return an empty JSON string with a 200 OK response.
  res.send('[]');
});

app.listen(3000, () => {
  console.log('Server running on port 3000.');
});