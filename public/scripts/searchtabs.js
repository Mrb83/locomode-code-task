/*
  Name: SearchTabs
  Type: Object creator, call SearchTabs() to create a new instance
  Description: Implements and encapsulates the tab widget used for flight search results
*/
var SearchTabs = function () {
    var attachedHeaderElement;
    var attachedContentElement;
    var headerTemplate;
    var contentTemplate;
    var tabCount = 0;

    function getTabAt(index) {
        if (index >= 0 && index < tabCount)
            return `#tab-${index}`;
        return undefined;
    }

    return {
        init: function () {
            // #id strings in this function should be user configurable in a real-world application
            // (e.g. via a settings object) rather than hardcoded
            attachedHeaderElement = '#flightTabHeader';
            attachedContentElement = '#flightTabContent';

            var headerSource = $('#tabHeaderTemplate').html();
            headerTemplate = Handlebars.compile(headerSource);
            var contentSource = $('#tabContentTemplate').html();
            contentTemplate = Handlebars.compile(contentSource);
        },

        clear: function() {
            $(attachedHeaderElement).empty();
            $(attachedContentElement).empty();
            tabCount = 0;
        },

        getTabCount: function () {
            return tabCount;
        },

        addTab: function (dateStr, isSearchDate) {
            var tabIndex = tabCount++;

            var data = {
                active: isSearchDate ? 'active' : '',
                searchDate: isSearchDate ? 'searchDate' : '',
                date: dateStr,
                id: `tab-${tabIndex}`
            };

            var headerHtml = headerTemplate(data);
            var contentHtml = contentTemplate(data);

            $(attachedHeaderElement).append(headerHtml);
            $(attachedContentElement).append(contentHtml);

            return tabIndex;
        },

        setTabContent: function(index, content) {
            var tab = getTabAt(index);
            if (tab)
                $(tab).empty().append(content);
        },

        setInvisible: function (invis) {
            $(attachedContentElement).toggleClass("invisible", invis);
            $(attachedHeaderElement).toggleClass("invisible", invis);
        }
    };
};