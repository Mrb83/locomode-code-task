/*
  Name: SearchController 
  Type: Singleton, call methods on SearchController directly.
  Description: Implements the high level controller of the search web application. Handles interaction between other lower-level components.
*/
SearchController = function () {
    var searchForm = SearchForm();
    var searchTabs = SearchTabs();

    function searchResultsReceived(data, context) {
        var html = SearchResultsGenerator.generate(data);
        searchTabs.setTabContent(context.tabIndex, html);
    }

    function searchRequested() {
        var searchData = searchForm.getData();
        var searchDate = searchData.date;

        var contextDates = SearchDateContext.getDates(searchDate);

        searchTabs.clear();

        for (var i = 0 ; i < contextDates.length ; ++i) {
            var tabIndex = searchTabs.addTab(contextDates[i], contextDates[i] == searchDate);

            var searchContext = {tabIndex: tabIndex};
            searchData.date = contextDates[i];

            SearchApi.getFlights(searchData, searchContext, searchResultsReceived);
        }
    }

    return {
        init: function () {
            searchForm.init();
            searchTabs.init();
            SearchResultsGenerator.init();

            // setup callback functions.
            searchForm.searchRequested(searchRequested);
            SearchApi.searchActive(searchTabs.setInvisible);
            SearchApi.searchActive(searchForm.setSearchActive);
        }
    };
} ();