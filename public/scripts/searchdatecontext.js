/*
  Name: SearchDateContext
  Type: Object, call methods on SearchDateContext directly.
  Description: Generates a list of context dates given a user-selected search date. Hardcoded to +/- 2 days for the test task but would be configurable in real-world app.
*/
var SearchDateContext = {
    getDates: function (date) {
        var dates = [];

        var searchDate = moment(date).subtract(2, 'd');
        if (!searchDate.isValid())
            return dates;

        for (var i = 0; i < 5; ++i) {
            var searchDateStr = searchDate.format('YYYY-MM-DD');
            dates.push(searchDateStr);
            searchDate = searchDate.add(1, 'd');
        }

        return dates;
    }
};