/*
  Name: SearchResultsGenerator
  Type: Singleton, call methods on SearchResultsGenerator directly.
  Description: Generates HTML flight results output from a JSON flight search object.
*/
var SearchResultsGenerator = function () {
    var resultTemplate;

    function formatFlightDuration(durationMin) {
        var duration = moment.duration(durationMin, 'minutes');
        var hours = duration.hours();
        var minutes = duration.minutes();

        var str = '';
        if (hours !== 0)
            str += `${hours}h`;

        str += `${minutes}m`;

        return str;
    }

    return {
        generate: function (data) {
            var html = '';
            var userMessage;
            var json;

            try {
                json = JSON.parse(data);
                if (!json.length)
                    userMessage = 'No flights found on date.';
            } catch (e) {
                userMessage = 'There was an error performing the search, please try again';
            }
            if (userMessage !== undefined) {
                html = `<div class="contentPlaceholder">${userMessage}</div>`;
                return html;
            }

            var even = false;
            json.forEach(function (value) {
                value.start.timeOfDay = moment.parseZone(value.start.dateTime).format('HH:mmA');
                value.finish.timeOfDay = moment.parseZone(value.finish.dateTime).format('HH:mmA');
                value.flightDuration = formatFlightDuration(value.durationMin);

                value.even = even ? " even" : "";
                even = !even;

                html += resultTemplate(value);
            });

            return html;
        },

        init: function () {
            // #id strings in this function should be user configurable in a real-world application
            // (e.g. via a settings object) rather than hardcoded
            var resultSource = $('#flightTemplate').html();
            resultTemplate = Handlebars.compile(resultSource);
        }
    };
} ();