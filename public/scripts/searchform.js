/*
  Name: SearchForm
  Type: Object creator, call SearchForm() to create a new instance
  Description: Binds to the individual elements of a search form, handles form validation, and notifies the outside world when a valid search request occurs.
*/
var SearchForm = function () {
    var fromAirportSelector =  AirportSelector();
    var toAirportSelector = AirportSelector();
    var departDateSelector = DateSelector();
    var searchRequestedCallbacks = $.Callbacks();
    var attachedSearchButton;
    var attachedSearchProgress;

    function isValid() {
        // Using alert() for form error display was so cool circa 1997.
        // But if I had more time, and in a real world app, I would
        // obviously do something better with inline HTML/CSS.

        if (!fromAirportSelector.isValid()) {
            alert('Please select a From location');
            return false;
        }
        if (!toAirportSelector.isValid()) {
            alert('Please select a To location');
            return false;
        }
        if (!departDateSelector.isValid()) {
            alert('Please select a departure date');
            return false;
        }
        return true;
    }

    function searchClicked(evt) {
        evt.preventDefault();

        if (!isValid())
            return;
        
        searchRequestedCallbacks.fire();
    }
    
    return {
        init: function() {
            // #id strings in this function should be user configurable in a real-world application
            // (e.g. via a settings object) rather than hardcoded
            attachedSearchButton = '#searchButton';
            attachedSearchProgress = '#searchProgress';

            $(attachedSearchButton).click(searchClicked);

            fromAirportSelector.init('#fromInput');
            toAirportSelector.init('#toInput');
            departDateSelector.init('#departDate');
        },

        searchRequested: function (callback) {
            searchRequestedCallbacks.add(callback);
        },

        getData: function() {
            return {
                from: fromAirportSelector.getAirport(),
                to: toAirportSelector.getAirport(),
                date: departDateSelector.getDate()
            };
        },

        setSearchActive: function(active) {
            $(attachedSearchButton).attr("disabled", active);
            $(attachedSearchProgress).toggleClass("invisible", !active);
        }
    };
};
