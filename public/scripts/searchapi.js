/*
  Name: SearchApi
  Type: Singleton, call methods on SearchApi directly.
  Description: Implements an interface to make search requests to the locomote flight API.
*/
var SearchApi = function () {
    var searchActiveCallbacks = $.Callbacks();
    var searchCount = 0;

    function incrementSearchCount()
    {
        ++searchCount;
        if (searchCount == 1)
          searchActiveCallbacks.fire(true);
    }

    function decrementSearchCount()
    {
        --searchCount;
        if (searchCount === 0)
            searchActiveCallbacks.fire(false);
    }

    return {
        getAirports: function (searchTerm, callback) {
            $.getJSON({
                url: "/airports",
                data: { q: searchTerm }
            }, callback);
            // XX Would do proper error checking/request tracking in real-world application
        },

        getFlights: function (data, context, callback) {
            incrementSearchCount();
            $.ajax({
                url: "/search",
                data: data,
                context: context,
                complete: function (res, textStatus) {
                    callback(res.responseText, this);

                    decrementSearchCount();
                }
            });
            // XX Would do proper error checking/request tracking in real-world application
        },

        searchActive: function(callback) {
            searchActiveCallbacks.add(callback);
        }
    };

} ();