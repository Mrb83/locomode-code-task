/*
  Name: DateSelector
  Type: Object creator, call DateSelector() to create a new instance
  Description: Binds to any input of type text and implements a date selecting (picking) widget
*/
var DateSelector = function () {
    var attachedElement;

    return {
        init: function (element) {
            attachedElement = element;
            $(attachedElement).datepicker({ dateFormat: 'yy-mm-dd' });
        },

        isValid: function () {
            var departDate = moment($(attachedElement).val());
            return departDate.isValid();
        },

        getDate: function() {
            return moment($(attachedElement).val()).format('YYYY-MM-DD');
        }
    };
};