/*
  Name: AirportSelector
  Type: Object creator, call AirportSelector() to create a new instance
  Description: Binds to any input of type text and implements airport search functionality
*/
var AirportSelector = function () {
    var attachedElement;
    var selectedAirport;

    var autoCompleteConfig = {
        minLength: 2,
        delay: 500,
        select: onAirportSelected,
        source: getAirports,
    };

    function resetDisplay() {
        var val = selectedAirport ? selectedAirport.label : '';
        $(attachedElement).val(val);
    }

    function onAirportSelected(event, ui) {
        event.preventDefault();

        if (ui.item.value != '(null)') {
            selectedAirport = {
                label: ui.item.label,
                code: ui.item.value
            };
        }

        resetDisplay();
    }

    function getAirports(req, res) {
        SearchApi.getAirports(req.term, function (airports) {
            var airportList = formatAirportList(airports);
            res(airportList);
        });
    }

    function formatAirportList(airports) {
        var airportList = airports.map(function (airport) {
            return {
                label: `${airport.cityName}, ${airport.countryCode} (${airport.airportCode})`,
                value: airport.airportCode
            };
        });

        if (!airportList.length)
            airportList = [{ label: 'No locations found.', value: '(null)' }];

        return airportList;
    }

    return {
        init: function (element) {
            attachedElement = element;
            $(attachedElement).autocomplete(autoCompleteConfig);
            $(attachedElement).focusout(resetDisplay);
        },

        isValid: function () {
            return selectedAirport && selectedAirport.code;
        },

        getAirport: function () {
            return selectedAirport.code;
        }
    };
};